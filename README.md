# Docker image for InfluxDB #

## Build the image ##

```shell
$> docker-compose up -d
```

## Access ##
```
http://localhost:8083/
```


## InfluxDB ##
More information about [InfluxData](https://influxdata.com/)